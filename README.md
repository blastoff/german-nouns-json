# German Nouns JSON from Wiktionary

The data that powers [Grammapp](https://grammapp.com/): 70,517 German nouns with grammar information and translations, parsed gratefully from the [Wiktionary](https://de.wiktionary.org) API.

Made available under the [Creative Commons Attribution-ShareAlike License](https://creativecommons.org/licenses/by-sa/3.0/).